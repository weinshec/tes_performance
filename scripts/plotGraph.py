#!/usr/bin/env python
# encoding: utf-8

""" plotGraph.py

desc:   Plot a std::vector tree branch entry as TGraph
date:   11-JAN-2017
author: weinshec
"""

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

import argparse
import matplotlib.pyplot as plt
import numpy as np

from tes_performance import io
from tes_performance.utils import argutils, plotutils



if __name__ == '__main__':

    # Create command line argument parser
    parser = argparse.ArgumentParser(
        description='Plot a std::vector tree branch entry')

    parser.add_argument('inputFile', metavar='<input>', type=str,
                        help='input root file')
    parser.add_argument('data', metavar='<tree:branch:entry>', type=str,
                        help='specify entry to plot')
    parser.add_argument('outputFile', metavar='output', nargs='?', type=str,
                        help='output plot file, interactive if none')

    argutils.add_plot_option_group(parser)

    args = parser.parse_args()

    # Get input tree
    treename, branch, entry = argutils.parse_combined_arg(
        args.data, [str, str, int])
    with io.PyTree(args.inputFile, treename) as tree:
        tree.GetEntry(entry)
        data = np.array(getattr(tree, branch))

    # Create the plot
    fig, ax = plt.subplots()
    ax.plot(np.arange(len(data)), data)
    plotutils.apply_plot_options(ax, args)

    # Output handling
    if args.outputFile:
        fig.savefig(args.outputFile)
    else:
        plt.show()
