#!/usr/bin/env python
# encoding: utf-8

""" plotSpectrum.py

desc:   Create a nice looking plot of a single spectrum.
date:   12-JAN-2017
author: weinshec
"""


import argparse
import matplotlib.pyplot as plt
import ROOT

from tes_performance import io
from tes_performance.utils import argutils, plotutils

ROOT.PyConfig.IgnoreCommandLineOptions = True
plotutils.apply_style("Atlas")


# Define default axis labels for all kinds of spectra
axis_labels = {
    'hPD': {
        'xaxis': 'frequency [Hz]',
        'yaxis': 'power spectrum [ $V_{rms}^{2}$ ]'
    },
    'hPSD': {
        'xaxis': 'frequency [Hz]',
        'yaxis': 'power spectral density / [ $V_{rms}^{2}/Hz$ ]'
    },
    'hLS': {
        'xaxis': 'frequency [Hz]',
        'yaxis': 'linear spectrum [ $V_{rms}$ ]'
    },
    'hLSD': {
        'xaxis': 'frequency [Hz]',
        'yaxis': 'linear spectral density / [ $V_{rms} / \sqrt{Hz}$ ]'
    },
}


if __name__ == '__main__':

    # Create command line argument parser
    parser = argparse.ArgumentParser(description='Plot a DFT spectrum')

    parser.add_argument('inputFile', metavar='<input>', type=str,
                        help='input root file')
    parser.add_argument('outputFile', metavar='output', nargs='?', type=str,
                        help='output plot file, interactive if none')

    argutils.add_plot_option_group(parser, axis_labels=False)

    args = parser.parse_args()

    # Open the input file
    with io.open_tfile(args.inputFile) as tfile:
        # Check which spectrum is in there and load the correct set of labels
        for key in ['hPS', 'hPSD', 'hLS', 'hLSD']:
            if tfile.GetListOfKeys().Contains(key):
                spectrum = tfile.Get(key)
                spectrum.SetDirectory(0)
                args.xlabel = axis_labels[key]['xaxis'] + \
                    " (bin width = {} Hz)".format(spectrum.GetBinWidth(1))
                args.ylabel = axis_labels[key]['yaxis']

    # Create a figure
    fig, ax = plt.subplots(figsize=(8, 6))
    plotutils.hist(spectrum, axes=ax, linewidth=2)
    plotutils.apply_plot_options(ax, args)
    ax.set_xscale("log")
    ax.set_yscale("log")

    # Output handling
    if args.outputFile:
        fig.savefig(args.outputFile)
    else:
        plt.show()
