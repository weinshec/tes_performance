#!/usr/bin/env python
# encoding: utf-8

""" plotContinuous.py

desc: plot timeseries of continuously recorded data
date: 11-JAN-2017
author: weinshec
"""

import argparse
import matplotlib.pyplot as plt
import ROOT

from tes_performance import io
from tes_performance.utils import argutils, plotutils

ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gSystem.Load("libfast.so")
plotutils.apply_style("Atlas")


if __name__ == '__main__':

    # Create commandline argument parser
    parser = argparse.ArgumentParser(
        description='Plot a std::vector tree branch entry as TGraph')

    parser.add_argument('inputFile', metavar='<input>', type=str,
                        help='input root file')
    parser.add_argument('data', metavar='<tree:branch>', type=str,
                        help='tree and branchname to plot')
    parser.add_argument('outputFile', metavar='output', nargs='?', type=str,
                        help='output plot file, interactive if none')

    argutils.add_plot_option_group(parser)

    args = parser.parse_args()

    # Read data
    treename, branch = argutils.parse_combined_arg(args.data, [str, str])
    with io.PyTree(args.inputFile, treename) as tree:
        fs = tree.samplingFrequency
        data = ROOT.fast.UnifiedVector("double")(tree, branch).data()
        time = ROOT.fast.utils.sampleTime("double")(len(data), fs)

    # Create the plot
    fig, ax = plt.subplots(figsize=(8, 6))
    ax.plot(time, data, linewidth=1.5)
    plotutils.apply_plot_options(ax, args)

    # Output handling
    if args.outputFile:
        fig.savefig(args.outputFile)
    else:
        plt.show()
