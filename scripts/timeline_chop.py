#!/usr/bin/env python
# encoding: utf-8

""" timeline_chop.py

desc:   Create AlazarRecord entries from a timeline using an offline trigger.
date:   09-AUG-2016
author: weinshec
"""

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
ROOT.gSystem.Load('libfast.so')

import argparse
import logging
import os


if __name__ == '__main__':

    # Create command line argument parser
    parser = argparse.ArgumentParser(description='Create AlazarRecord entries\
            from a timeline using an offline trigger.')
    parser.add_argument('--debug', action='store_true',
                        help='enable debug output')
    chArg = parser.add_mutually_exclusive_group(required=True)
    chArg.add_argument('--ChA', action='store_true',
                       help='Select data from Channel A')
    chArg.add_argument('--ChB', action='store_true',
                       help='Select data from Channel B')
    parser.add_argument('trigLvl', metavar='<trigLvl>', type=float,
                        help='trigger level given in Volts')
    parser.add_argument('inputFile', metavar='<input>', type=str,
                        help='input timeline root file')
    parser.add_argument('outputFile', metavar='<output>', type=str,
                        help='output root file')
    parser.add_argument('--pre', metavar='INT', type=int, default=500,
                        help='number of presamples to store [Default: 500]')
    parser.add_argument('--post', metavar='INT', type=int, default=500,
                        help='number of postsamples to store [Default: 500]')
    args = parser.parse_args()

    # Set logging options
    logLevel = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(format='[%(levelname)s]: %(message)s', level=logLevel)

    # Get a shrared_ptr to tree
    logging.info("Reading file: %s" % os.path.basename(args.inputFile))
    ROOT.gROOT.ProcessLine('file = TFile("'+args.inputFile+'","READ");\
        tree = std::shared_ptr<TTree>((TTree*) file.Get("alazar_data"));')

    # Extract sampling frequency
    for event in ROOT.file.Get("alazar_data"):
        fs = event.samplingFrequency
        logging.debug("Found sampling frequency: %.1f Hz" % fs)
        break

    # Scan for trigger events
    ch = 'dataChA' if args.ChA else 'dataChB'
    logging.info("Scanning timeline of %s with TL=%.3f V ..."
        % (ch, args.trigLvl))
    tlt = ROOT.fast.TimelineTrigger(ROOT.tree, ch)
    tlt.setRecordSamplingFreq(fs)
    tlt.setRecordChannel(ROOT.fast.ChA if args.ChA else ROOT.fast.ChB)
    nTriggers = tlt.scan(args.trigLvl, 0, args.post)
    logging.debug("... found %d trigger points" % nTriggers)

    # Create output file and tree
    logging.info("Creating output file: %s" % args.outputFile)
    outFile = ROOT.TFile(args.outputFile, 'RECREATE')
    outTree = ROOT.tree.CloneTree(0)

    record = ROOT.fast.AlazarRecord()
    outTree.SetBranchAddress('dataChA',
            ROOT.AddressOf(record, 'dataChA'))
    outTree.SetBranchAddress('triggerChA',
            ROOT.AddressOf(record, 'triggerChA'))
    outTree.SetBranchAddress('dataChB',
            ROOT.AddressOf(record, 'dataChB'))
    outTree.SetBranchAddress('triggerChB',
            ROOT.AddressOf(record, 'triggerChB'))
    outTree.SetBranchAddress('timeStamp',
            ROOT.AddressOf(record, 'timeStamp'))
    outTree.SetBranchAddress('samplingFrequency',
            ROOT.AddressOf(record, 'samplingFrequency'))

    for eventPos in tlt.getPeakPositions():
        new_record = tlt.extractRecord(eventPos, args.pre, args.post)

        record.dataChA = new_record.dataChA
        record.dataChB = new_record.dataChB
        record.triggerChA = new_record.triggerChA
        record.triggerChB = new_record.triggerChB
        record.samplingFrequency = new_record.samplingFrequency
        record.timeStamp = new_record.timeStamp

        outTree.Fill()

    outFile.cd()
    outTree.Write()
