/**
 * peakfinding.cpp - Apply peakfinding algorithms on _simulated_ TES events
 *
 * author: Christoph Weinsheimer
 * email: christoph.weinsheimer@desy.de
 * description: Apply a PeakFindingUnit processor on TES events storing the
 *   result and the according truth information in an output record.
 */

#include <numeric>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <fast/utils.h>
#include <fast/processor.h>

#include "TFile.h"
#include "TMath.h"
#include "TTree.h"
#include "TH2D.h"

#include "common.h"
#include "version.h"


using namespace fast;
using ConfigParser = fast::utils::ConfigParser;


int main(int argc, char* argv[]) {
  common::printInfo(argc, argv);
  utils::printLibVersion();
  utils::Logger::instance()->setLevel(utils::LogLevel::DEBUG);

  if (argc != 4) {
    std::cout << "Syntax: peakfinding <input> <config> <output>" << std::endl;
    return 1;
  }

  // Parse configuration
  auto config = ConfigParser::parseJson(argv[2]);

  // Read input file
  TFile infile(argv[1], "READ");
  std::shared_ptr<TTree> inTree((TTree*) infile.Get("alazar_data"));

  // Create output file and set branches to copy
  TFile outfile(argv[3], "RECREATE");
  common::DerivedTree derivedOutTree(inTree);
  derivedOutTree.addBranchByName("dataChA");
  derivedOutTree.addBranchByName("samplingFrequency");
  derivedOutTree.addBranchByName("triggerChA");
  derivedOutTree.addBranchByRegex("^truth_.*");
  std::shared_ptr<TTree> outTree = derivedOutTree.get("processed_data");

  // Create TreeReader
  TreeReader<AlazarRecord> treeReader(inTree);

  // Create processors
  const Channel ch = Channel::ChA;

  // Parse treshold section, if any
  double threshold = 0;
  if (config.count("threshold")) {

    auto noise_ref = ConfigParser::parseJson(
      utils::expandEnv(config["threshold"]["reference"]));
    threshold = config["threshold"]["value"];

    std::stringstream param_key;
    param_key << std::fixed << std::setprecision(2);

    switch (config["method"].get<unsigned int>()) {
      case PeakFindingUnit::DERIV:
        param_key << config["params"]["cutOff"].get<float>();
        threshold *= noise_ref["deriv"][param_key.str()]["sigma"].get<double>();
        break;
      case PeakFindingUnit::XCORR:
        param_key << config["params"]["tMinFactor"].get<float>();
        threshold *= noise_ref["xcorr"][param_key.str()]["sigma"].get<double>();
        break;
    }
    utils::LogInfo("peakfinding") << "Using threshold" << threshold;
  }


  std::unique_ptr<PeakFindingUnit> pfu;
  switch (config["method"].get<unsigned int>()) {
    case PeakFindingUnit::DERIV:
      pfu = std::unique_ptr<PeakFindingUnit>(
          new DerivProcessor("L2", ch, config["params"]["cutOff"], threshold));
      break;
    case PeakFindingUnit::XCORR:
      pfu = std::unique_ptr<PeakFindingUnit>(
          new XCorrProcessor("L2", ch, PulseShape::PHOTON_55mV(),
                             config["params"]["tMinFactor"], threshold));
      break;
  }
  pfu->setTree(outTree);

  // Event loop
  for (size_t i = 0; i < treeReader.size(); ++i) {
    auto event  = treeReader.getEntry(i);

    (*pfu)(event);

    outTree->Fill();
  }

  outfile.cd();
  outTree->Write();

  return 0;
}
