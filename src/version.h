#ifndef __TES_PERFORMANCE_VERSION__
#define __TES_PERFORMANCE_VERSION__


namespace common {

extern const char GIT_VERSION[];
extern const char GIT_HASH[];

void printInfo(int argc=0, char* argv[]=nullptr);

} /* namespace common */


#endif /* __TES_PERFORMANCE_VERSION__ */
