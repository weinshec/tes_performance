/**
 * pulsefitting.cpp - Apply PulseFitting PU
 *
 * author: Christoph Weinsheimer
 * email: christoph.weinsheimer@desy.de
 */
#include <iostream>

#include <fast/utils.h>
#include <fast/processor.h>
#include <fast/pulsefct.h>

#include "TFile.h"
#include "TMath.h"
#include "TTree.h"

#include "common.h"
#include "version.h"


using namespace fast;
using ConfigParser = fast::utils::ConfigParser;


int main(int argc, char* argv[]) {
  common::printInfo(argc, argv);
  utils::printLibVersion();
  utils::Logger::instance()->setLevel(utils::LogLevel::DEBUG);

  if (argc != 4) {
    std::cout << "Syntax: pulsefitting <input> <config> <output>" << std::endl;
    return 1;
  }

  // Parse configuration
  auto config = ConfigParser::parseJson(argv[2]);
  const Channel ch = Channel::ChA;

  // Read input file
  TFile infile(argv[1], "READ");
  std::shared_ptr<TTree> inTree((TTree*) infile.Get("alazar_data"));

  // Create output file
  TFile outfile(argv[3], "RECREATE");
  common::DerivedTree derivedOutTree(inTree);
  derivedOutTree.addBranchByName("dataChA");
  derivedOutTree.addBranchByName("samplingFrequency");
  derivedOutTree.addBranchByName("triggerChA");
  derivedOutTree.addBranchByRegex("^truth_.*");
  std::shared_ptr<TTree> outTree = derivedOutTree.get("processed_data");
  auto plotDir = outfile.mkdir("failed");
  plotDir->cd();

  // Create peakfinding unit
  std::shared_ptr<PeakFindingUnit> pfu = nullptr;
  double threshold = config["threshold"]["value"];
  auto noise_ref = ConfigParser::parseJson(
    utils::expandEnv(config["threshold"]["reference"]));
  std::stringstream param_key;
  param_key << std::fixed << std::setprecision(2);

  switch (config["method"].get<unsigned int>()) {
    case PeakFindingUnit::DERIV:
      param_key << config["params"]["cutOff"].get<float>();
      threshold *= noise_ref["deriv"][param_key.str()]["sigma"].get<double>();
      pfu = std::make_shared<DerivProcessor>(
        "L2", ch, config["params"]["cutOff"], threshold
      );
      break;
    case PeakFindingUnit::XCORR:
      param_key << config["params"]["tMinFactor"].get<float>();
      threshold *= noise_ref["xcorr"][param_key.str()]["sigma"].get<double>();
      pfu = std::make_shared<XCorrProcessor>(
        "L2", ch, PulseShape::PHOTON_55mV(), config["params"]["tMinFactor"],
        threshold
      );
      break;
  }
  pfu->setTree(outTree);

  // Create Processors
  PulseFitProcessor pu_fit("fit", ch, 0.005, pfu);
  pu_fit.setTree(outTree);
  PHPIProcessor     pu_phpi("phpi", ch, true);
  pu_phpi.setTree(outTree);

  // Create TreeReader
  TreeReader<AlazarRecord> treeReader(inTree);

  // Create additional branches
  double fit_integral;
  outTree->Branch("fit_integral",    &fit_integral);

  // Event loop
  for (size_t i = 0; i < treeReader.size(); ++i) {
    utils::LogInfo("pulsefitting") << i;
    auto event = treeReader.getEntry(i);

    auto fitRecord  = pu_fit(event);
    auto phpiRecord = pu_phpi(event);

    fit_integral  = pulsefct::pulse_integral(fitRecord.amplitude[0],
                                             fitRecord.tRise[0],
                                             fitRecord.tFall[0]);

    if (!fitRecord.fitResult->IsValid()) {
      utils::LogWarning("methodComp")
        << "Fit failed! Event " + std::to_string(i);
      fitRecord.graph->Write(utils::add_suffix("failed", i).c_str());
    }

    outTree->Fill();
  }

  outfile.cd();
  outTree->Write();

  return 0;
}
