#include "common.h"

#include <regex>
#include <stdexcept>

namespace common {

DerivedTree::DerivedTree(const std::shared_ptr<TTree>& original)
    : original_(original) {}

void DerivedTree::addBranchByName(const std::string& branchName) {
  if (branchExists(branchName)) branches_.push_back(branchName);
  else throw std::runtime_error("Branch does not exist " + branchName);
}

void DerivedTree::addBranchByRegex(const std::string& expr) {
  std::regex re(expr);
  std::smatch match;

  for (const auto& branch: *original_->GetListOfBranches()) {
    std::string branchName(branch->GetName());
    if (std::regex_search(branchName, match, re))
      branches_.push_back(branchName);
  }
}

std::unique_ptr<TTree> DerivedTree::get(const std::string& name) const {
  original_->SetBranchStatus("*", 0);

  for (const auto& branchName : branches_)
    original_->SetBranchStatus(branchName.c_str(), 1);

  std::unique_ptr<TTree> tree(original_->CloneTree(0));
  tree->SetNameTitle(name.c_str(), name.c_str());
  original_->SetBranchStatus("*", 1);
  return tree;
}

bool DerivedTree::branchExists(const std::string& branchName) const {
  auto branchPtr = original_->GetBranch(branchName.c_str());
  return branchPtr != nullptr;
}

} /* namespace common */
