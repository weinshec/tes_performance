/**
 * tesmc.cpp - TESMC Monte-Carlo generator for TES events
 *
 * author: Christoph Weinsheimer
 * email: christoph.weinsheimer@desy.de
 * description: Create samples of TES events using the Monte-Carlo method. The
 *   details of the simulation run are specified in the config file.
 */

#include <iostream>

#include <fast/utils.h>
#include <fast/generator.h>

#include "TFile.h"
#include "TTree.h"

#include "version.h"


using namespace fast;
using ConfigParser = fast::utils::ConfigParser;


int main(int argc, char* argv[]) {
  common::printInfo(argc, argv);
  utils::printLibVersion();
  utils::Logger::instance()->setLevel(utils::LogLevel::INFO);

  if (argc != 3) {
    std::cout << "Syntax: generate <configFile> <outputFile>" << std::endl;
    return 1;
  }

  // Default configuration
  auto config = ConfigParser::parseJson(argv[1]);
  std::cout << "Using configuration:" << std::endl
            << std::setw(2) << config << std::setw(0) << std::endl;

  // Create output file
  TFile outfile(argv[2], "RECREATE");
  auto tree = std::make_shared<TTree>("alazar_data", "alazar_data");

  // Configure Generator
  const Channel ch = Channel::ChA;
  Generator gen(config["sampleSize"], config["samplingFreq"]);
  gen.setTree(tree);

  // Add photons
  unsigned int nPhoton = 0;
  for (auto photon : config["photons"]) {
    gen.addTransformation<AddPhotonPulse>( ch,
        Parameter::fromJSON(utils::add_suffix("truth_amplitude", nPhoton),
                            photon["amplitude"]),
        Parameter::fromJSON(utils::add_suffix("truth_tRise", nPhoton),
                            photon["tRise"]),
        Parameter::fromJSON(utils::add_suffix("truth_tFall", nPhoton),
                            photon["tFall"]),
        Parameter::fromJSON(utils::add_suffix("truth_tOffset", nPhoton),
                            photon["tOffset"]));
    ++nPhoton;
  }

  // Add noise
  auto noise = config["noise"];
  if (noise.find("spectral") != noise.end()) {
    gen.addTransformation<AddSpectralNoise>( ch,
        utils::expandEnv(noise["spectral"]),
        "hLSD", SpectrumType::LSD);
  }
  if (noise.find("white") != noise.end()) {
    gen.addTransformation<AddWhiteNoise>( ch,
        Parameter::fromJSON("noise_stddev", noise["white"]));
  }

  // Generate samples
  for (size_t n = 0; n < config["nEvents"].get<size_t>(); ++n) {
    gen.generate();
    tree->Fill();

    if (n%1000 == 0)
      utils::LogInfo("tesmc") << n;
  }

  outfile.cd();
  tree->Write();

  return 0;
}
