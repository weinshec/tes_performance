#ifndef __TES_PERFORMANCE_COMMON__
#define __TES_PERFORMANCE_COMMON__

#include <memory>
#include <vector>

#include "TTree.h"


namespace common {

class DerivedTree {
 public:
  DerivedTree(const std::shared_ptr<TTree>& original);

  void addBranchByName(const std::string& branchName);
  void addBranchByRegex(const std::string& expr);

  std::unique_ptr<TTree> get(const std::string& name) const;

 private:
  const std::shared_ptr<TTree> original_;
  std::vector<std::string> branches_;

  bool branchExists(const std::string& branchName) const;
};

} /* namespace common */


#endif /* __TES_PERFORMANCE_COMMON__ */
