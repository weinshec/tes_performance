#!/usr/bin/env python
# encoding: utf-8

import logging
import os
import ROOT


class PyTree(object):
    """Read trees from ROOT files in a more pythonic way"""

    def __init__(self, filepath, treename):
        """Initialize a TreeReader object

        :param str filepath: path to the root file to open
        :param str treename: name of the TTree to read
        """
        self._filepath = filepath
        self._treename = treename

    def __enter__(self):
        """Context manager enter policy. Open the TFile and retrieve the tree.
        """
        logging.debug("Reading Tree \"{}\" from {}".format(
            self._treename, os.path.basename(self._filepath)))
        self._open_file = ROOT.TFile(self._filepath, "read")
        self._tree = self._open_file.Get(self._treename)
        if self._tree.GetEntriesFast() > 0:
            self._tree.GetEntry(0)
        return self._tree

    def __exit__(self, *args):
        """Context manager exit policy. Close the TFile.
        """
        self._tree = None
        self._open_file.Close()


class open_tfile(object):
    """Open ROOT files in a more pythonic way"""

    def __init__(self, filepath, mode="read"):
        """Initialize a PyTFile object

        :param str filepath: path to the root file to open
        """
        self._filepath = filepath
        self._mode = mode

    def __enter__(self):
        """Context manager enter policy. Open the TFile.
        """
        logging.debug("Opening TFile {}".format(
            os.path.basename(self._filepath)))
        self._open_file = ROOT.TFile(self._filepath, self._mode)
        return self._open_file

    def __exit__(self, *args):
        """Context manager exit policy. Close the TFile.
        """
        self._open_file.Close()
