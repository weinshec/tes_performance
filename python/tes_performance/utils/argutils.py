#!/usr/bin/env python
# encoding: utf-8


def add_plot_option_group(argparser,
                          axis_scale=True, axis_labels=True,
                          label=True, grid=True):

    group = argparser.add_argument_group("Plotting options")

    if axis_scale:
        group.add_argument("--xscale", metavar="low:high", type=str,
                           help="set x axis scale")
        group.add_argument("--yscale", metavar="low:high", type=str,
                           help="set y axis scale")

    if axis_labels:
        group.add_argument("--xlabel", metavar="str", type=str, default="",
                           help="set x axis label")
        group.add_argument("--ylabel", metavar="str", type=str, default="",
                           help="set y axis label")

    if label:
        group.add_argument("--label", metavar="STR", type=str,
                           help="optional label in upper right corner")

    if grid:
        group.add_argument("--grid", action="store_true", default=False,
                           help="draw grid lines")


def parse_combined_arg(arg, dtypes, split=":"):
    parsed_items = list()
    tokens = arg.split(split)

    if len(tokens) != len(dtypes):
        raise AttributeError("Need to provide as much dtypes as arguments")

    for dtype, token in zip(dtypes, tokens):
        try:
            parsed_items.append(dtype(token))
        except:
            raise TypeError(f"Cannot convert \"{token}\" to {dtype}")

    return parsed_items
