#!/usr/bin/env python
# encoding: utf-8

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from tes_performance.utils import argutils
from root_numpy import hist2array


def apply_plot_options(ax, args):
    format_axis(ax, args.xlabel, args.ylabel)

    if args.xscale:
        ax.set_xlim(argutils.parse_combined_arg(args.xscale, [float, float]))
    if args.yscale:
        ax.set_ylim(argutils.parse_combined_arg(args.yscale, [float, float]))

    if args.label:
        add_label(ax, args.label)

    if args.grid:
        ax.grid()


def format_axis(ax, xlabel=None, ylabel=None):
    if xlabel:
        ax.set_xlabel(xlabel, position=(1., 0.), va='top', ha='right')
    if ylabel:
        ax.set_ylabel(ylabel, position=(0., 1.), va='bottom', ha='right')


def add_label(ax, label, x=0.96, y=0.95, size=19, **kwargs):
    ax.text(x, y, label, transform=ax.transAxes, va="top", ha="right",
            fontsize=size, **kwargs)


def hist(th1, axes, **kwargs):
    contents, edges = hist2array(th1, return_edges=True)

    if kwargs.pop('fill', False):
        axes.fill_between(
            edges[0], 0, [*contents, contents[-1]],
            step="post",
            linewidth=0,
            facecolor=kwargs.get("facecolor", kwargs.get("color")),
            hatch=kwargs.get("hatch"),
            alpha=kwargs.get("alpha"),
        )
    return axes.step(
        edges[0], [*contents, contents[-1]],
        where="post",
        label=kwargs.get("label"),
        linewidth=kwargs.get("linewidth"),
        alpha=kwargs.get("alpha"),
        color=kwargs.get("color"),
    )


def hist2d(th2, axes, colorbar=True, **kwargs):
    contents, (edges_x, edges_y) = hist2array(th2, return_edges=True)
    x, y = np.meshgrid(edges_x[:-1], edges_y[:-1])

    counts, xedges, yedges, image = axes.hist2d(
        np.ravel(x), np.ravel(y),
        weights=np.ravel(contents.T),
        bins=(edges_x, edges_y),
        **kwargs)

    if colorbar:
        plt.colorbar(image)

    return counts, xedges, yedges, image


def errorbar(th1, axes, xerr=True, **kwargs):
    contents, edges = hist2array(th1, return_edges=True)
    xedges = edges[0]
    centers = (xedges[:-1] + xedges[1:])/2

    y_errors = np.array([th1.GetBinError(i+1) for i in range(th1.GetNbinsX())])
    x_errors = None if not xerr else np.array([
        th1.GetBinWidth(i+1)/2 for i in range(th1.GetNbinsX())
    ])

    defaults = {
        "color": "black",
        "marker": "o",
        "linestyle": "",
        "capsize": 0,
    }
    for k, v in defaults.items():
        kwargs.setdefault(k, v)

    return axes.errorbar(centers, contents, xerr=x_errors, yerr=y_errors,
                         **kwargs)
