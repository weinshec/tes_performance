#!/usr/bin/env python
# encoding: utf-8

import json
import os

from tes_performance import tes_performance_data


method = {
    1: "deriv",
    2: "xcorr",
}


def get_noise_ref(methodID, params,
                  ref_file="peakfinding.tesmc.50MSPS.spectralNoise_CD30.json"):
    """Get the peakfinding noise reference for a given method and its
    parameters.

    :param int methodID: id of the peakfinding method
    :param list params: a list of parameters used to specify the given method
    :param str ref_file: filename of the noise reference file to use
    :return: noise reference sigma
    :rtype: float
    """

    filepath = os.path.join(tes_performance_data, "peakfinding")
    with open(os.path.join(filepath, ref_file)) as j:
        ref = json.load(j)

    if methodID == 1:
        cutoff = "{:.2f}".format(params[0])
        return ref[method[methodID]][cutoff]["sigma"]
    if methodID == 2:
        tMin = "{:.2f}".format(params[0])
        return ref[method[methodID]][tMin]["sigma"]
