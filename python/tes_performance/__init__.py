import logging
import os

tes_performance_data = os.path.expandvars("${TES_PERFORMANCE_DATA}")

if not os.path.exists(tes_performance_data) or \
   not os.path.isdir(tes_performance_data):
    logging.warning("$TES_PERFORMANCE_DATA not found or not set")
