#!/usr/bin/env python
# encoding: utf-8

import numpy as np


def closest_match(value, vector):
    """Find the closest matching element within a vector and return its index
    and the absolute value of the remaining distance.

    :param float value: value to search for
    :param list vector: list of elements to search in
    :return: tuple containing the closest index and the remaining distance
    :rtype: tuple
    """
    closest_i = None
    closest_v = np.inf

    for i, v in enumerate(vector):
        distance = np.abs(v - value)
        if distance < np.abs(closest_v - value):
            closest_v = v
            closest_i = i

    return closest_i, np.abs(closest_v - value)


def closest_vector_match(v0, v1, max_distance):
    """Find the indices of all close matching elements within two vectors
    respecting a finite tolerance distance.

    All elements of one list are compared to all elements of the second list
    and the absolute distance is calculated. If that distance is smaller than
    `max_distance` the corresponing indices within the vectors are stored.

    :param list v0: first vector
    :param list v1: second vector
    :return: tuple of matching list indices in `v0` and `v1`
    :rtype: tuple
    """
    m0 = []
    m1 = []
    for i, val in enumerate(v0):
        idx, d = closest_match(val, v1)
        if d <= max_distance and idx not in m1:
            m0.append(i)
            m1.append(idx)
    return m0, m1
