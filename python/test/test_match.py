import unittest

from tes_performance.match import closest_match, closest_vector_match


class Test_match(unittest.TestCase):

    def setUp(self):
        self.test_vector = [0.1, 3.0, 7.0, 7.5]

    def test_closest_match(self):
        i, d = closest_match(2.0, self.test_vector)
        self.assertEqual((i, d), (1, 1.0))

    def test_closest_vector_match_exact_mathes(self):
        m0, m1 = closest_vector_match(v0=[0.1, 7.0],
                                      v1=self.test_vector,
                                      max_distance=0.05)
        self.assertListEqual(m0, [0, 1])
        self.assertListEqual(m1, [0, 2])

    def test_closest_vector_match_exact_matches_vice_versa(self):
        m0, m1 = closest_vector_match(v1=[0.1, 7.0],
                                      v0=self.test_vector,
                                      max_distance=0.05)
        self.assertListEqual(m1, [0, 1])
        self.assertListEqual(m0, [0, 2])

    def test_closest_vector_match_close_matches(self):
        m0, m1 = closest_vector_match(v0=[0.11, 6.99],
                                      v1=self.test_vector,
                                      max_distance=0.05)
        self.assertListEqual(m0, [0, 1])
        self.assertListEqual(m1, [0, 2])

    def test_closest_vector_match_single_mismatch(self):
        m0, m1 = closest_vector_match(v0=[0.11, 5.0],
                                      v1=self.test_vector,
                                      max_distance=0.05)
        self.assertListEqual(m0, [0])
        self.assertListEqual(m1, [0])

    def test_closest_vector_match_nothing_mathing(self):
        m0, m1 = closest_vector_match(v0=[1.0, 5.0],
                                      v1=self.test_vector,
                                      max_distance=0.05)
        self.assertListEqual(m0, [])
        self.assertListEqual(m1, [])

    def test_closest_vector_match_no_double_counting(self):
        m0, m1 = closest_vector_match(v0=[3.0],
                                      v1=[1.0, 2.99, 3.01, 5.0],
                                      max_distance=0.05)
        self.assertListEqual(m0, [0])
        self.assertListEqual(m1, [1])

    def test_closest_vector_match_no_double_counting_vice_versa(self):
        m0, m1 = closest_vector_match(v1=[3.0],
                                      v0=[1.0, 2.99, 3.01, 5.0],
                                      max_distance=0.05)
        self.assertListEqual(m1, [0])
        self.assertListEqual(m0, [1])
