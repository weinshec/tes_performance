#!/usr/bin/env python

from pathlib import Path
from setuptools import setup

root = Path(__file__).parent or "."

with (root / "requirements.txt").open(encoding="utf-8") as f:
    requirements = list(filter(None, (row.strip() for row in f)))


info = {
    "name":                "tes_performance",
    "version":             "0.0.1",
    "author":              "Christoph Weinsheimer",
    "author_email":        "christoph.weinsheimer@desy.de",
    "packages":            ["tes_performance"],
    "provides":            ["tes_performance"],
    "license":             "GNU Lesser General Public License (LGPL) v3.0",
    "description":         "Analysing methods for the TES Performance project",
    "long_description":    "Analysing methods for the TES Performance project",
    "install_requires":    requirements,
    "setup_requires":      ["nose", ],
}

if __name__ == "__main__":
    setup(**info)
