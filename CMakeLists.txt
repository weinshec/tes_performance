###############################################################################
cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)
project("tes_performance")
###############################################################################

#______________________________________________________________________________
#                                                                         DEBUG
message( STATUS "Include debug symbols with -DCMAKE_BUILD_TYPE=Debug")
set(CMAKE_C_FLAGS_DEBUG "-ggdb -O0")
set(CMAKE_CXX_FLAGS_DEBUG "-ggdb -O0")
set(CMAKE_C_FLAGS_RELWITHDEBINFO "-ggdb -O0")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-ggdb -O0")


#______________________________________________________________________________
#                                                                          ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
find_package(ROOT REQUIRED COMPONENTS Core RIO Net Hist Graf Tree MathCore)
include(${ROOT_USE_FILE})
add_definitions(${ROOT_CXX_FLAGS})


#______________________________________________________________________________
#                                                                    GITVERSION
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/gitversion/")
include(GetGitRevisionDescription)
get_git_head_revision(GIT_REFSPEC GIT_HASH)
git_describe(GIT_VERSION)
MESSAGE(STATUS "git version: ${GIT_VERSION}")


#______________________________________________________________________________
#                                                                          FAST
find_package(fast REQUIRED)


#______________________________________________________________________________
#                                                                      INCLUDES
include_directories(${CMAKE_SOURCE_DIR}
                    ${CMAKE_SOURCE_DIR}/src
                    ${FAST_INCLUDE_DIRS}
                    ${ROOT_INCLUDE_DIRS})


#______________________________________________________________________________
#                                                                         FLAGS
add_definitions("-Wall -Wextra -Wno-long-long")
set(CMAKE_CXX_STANDARD 14)


#______________________________________________________________________________
#                                                                       TARGETS
add_subdirectory(src)
add_subdirectory(scripts)
